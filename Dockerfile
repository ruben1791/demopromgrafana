FROM openshift/redhat-openjdk18-openshift:1.4

ADD ./app/target/test-app-1.0.0.jar /app/app.jar

ENTRYPOINT ["java","-jar","/app/app.jar"]