package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;


@SpringBootApplication
@RestController
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}



	@RequestMapping("/hola")
	public String hola() {
		return "Hola";
	}

	@PostMapping("/saludos")
	public String saludos(@RequestParam("nombre") String nombre) {
		return "Hola "+nombre;
	}

	@RequestMapping("/adios")
	public String adios() {
		return "Adios";
	}


}
